return {
  --  { "shaunsingh/nord.nvim" },
  {
    "gbprod/nord.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("nord").setup({
        transparent = true,
        search = { theme = "vim" },
        borders = true,
        styles = {
          comments = { italic = true },
          functions = { italic = true },
          bufferline = {
            modified = { italic = true },
          },
        },
      })
      vim.cmd.colorscheme("nord")
    end,
  },

  --  {
  --    "LazyVim/LazyVim",
  --    opts = {
  --      colorscheme = "nord",
  --    },
  --  },
}
